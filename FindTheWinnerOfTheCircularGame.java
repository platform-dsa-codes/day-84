class Solution {
    public int findTheWinner(int n, int k) {
        int winner = 0; // Initialize the winner to 0-based index
        for (int i = 1; i <= n; i++) {
            winner = (winner + k) % i;
        }
        return winner + 1; // Convert 0-based index to 1-based index
    }
}
